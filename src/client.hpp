/*
 * client.h
 *
 *  Created on: Feb 26, 2018
 *      Author: walaryne
 */

#ifndef CLIENT_HPP_
#define CLIENT_HPP_

#define MESSAGE_SIZE 1024
#define HEADER_SIZE 4
#define PACKET_SIZE (MESSAGE_SIZE + HEADER_SIZE)
#include <iostream>
#ifdef __WIN32__
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#pragma comment (lib, "Ws2_32.lib")
#else
#include <netdb.h>
#endif
#include <gtkmm.h>
#include "gui.hpp"

class Client {
public:
	int Initialize(std::string, std::string);
	int Send(const char *, int);
	void Receive();
	addrinfo* GetServerStruct();
	Glib::Dispatcher dispatcher;
	void SharedQueuePush(std::string);
	void SharedQueuePop();
	std::string SharedQueueGet();
	bool IsPrintQueueEmpty();
	bool IsThreadRunning();
	void SetIsThreadRunning(bool);
	void DisconnectSocket();
	bool ConnectSocket(addrinfo *);
	Client();
	virtual ~Client();
private:
	std::deque<std::string> print_queue_;
	addrinfo *servinfo_;
	int sockfd_;
	bool connected_;
	bool running_;
};

#endif /* CLIENT_HPP_ */
