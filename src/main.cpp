/*
 ============================================================================
 Name        : main.cpp
 Author      : Walaryne
 Version     :
 Copyright   : (c) Walaryne, 2018
 Description : Farside, a GUI based chat program.
 ============================================================================
 */

#include <iostream>
#include <thread>
#include <mutex>
#include <gtkmm.h>
#include <string.h>
#ifdef __WIN32__
#include <winsock2.h>
#endif
#include "gui.hpp"
#include "client.hpp"

Gui *g = nullptr;

int main(int argc, char *argv[]) {
#ifdef __WIN32__
	WORD versionWanted = MAKEWORD(2, 0);
	WSADATA wsaData;
	WSAStartup(versionWanted, &wsaData);
#endif
	auto app = Gtk::Application::create(argc, argv, "org.gtkmm.examples.base");
	auto refBuilder = Gtk::Builder::create();
	try {
		refBuilder->add_from_file("FarsideUI.glade");
	}
	catch(const Glib::FileError& ex) {
		std::cerr << "FileError: " << ex.what() << std::endl;
		return 1;
	}
	catch(const Glib::MarkupError& ex) {
		std::cerr << "MarkupError: " << ex.what() << std::endl;
		return 1;
	}
	catch(const Gtk::BuilderError& ex) {
		std::cerr << "BuilderError: " << ex.what() << std::endl;
		return 1;
	}
	refBuilder->get_widget_derived("window1", g);
	if(g) {
		refBuilder->get_widget("button1", g->button_);
		refBuilder->get_widget("entry1", g->entry_);
		refBuilder->get_widget("textview1", g->textview_);
		refBuilder->get_widget("scrolledwindow1", g->scrolledwindow_);
		g->textviewbuffer_ = g->textview_->get_buffer();
		g->adj_ = g->scrolledwindow_->get_vadjustment();
		if(g->button_) {
			g->button_->signal_clicked().connect( sigc::mem_fun(g, &Gui::parseInput) );
		}
		if(g->entry_) {
			g->entry_->signal_activate().connect( sigc::mem_fun(g, &Gui::parseInput) );
			g->entry_->add_events(Gdk::KEY_PRESS_MASK);
		}
		app->set_flags(Gio::APPLICATION_NON_UNIQUE);
		app->run(*g);
	}
	delete g;
	return 0;
}
