/*
 * util.cpp
 *
 *  Created on: Feb 26, 2018
 *      Author: walaryne
 */


#include <iostream>

//Simple convenience function for printing strings to stdout, with an affixed newline.
int cnprint(std::string s) {
	try {
		std::cout << s << "\n" << std::endl;
		return 0;
	} catch(std::exception &e) {
		std::cerr << e.what() << "\n" << std::endl;
		return 1;
	}
}

int cnprinte(std::string s) {
	try {
		std::cerr << s << "\n" << std::endl;
		return 0;
	} catch(std::exception &e) {
		std::cerr << e.what() << "\n" << std::endl;
		return 1;
	}
}
