/*
 * gui.hpp
 *
 *  Created on: Mar 3, 2018
 *      Author: walaryne
 */

#ifndef GUI_HPP_
#define GUI_HPP_

#include <gtkmm.h>
#include <deque>
#include "client.hpp"

class Gui : public Gtk::Window {
public:
	Gui(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& builder);
	virtual ~Gui();
	void parseInput();
	Gtk::Button *button_ = nullptr;
	Gtk::Entry *entry_ = nullptr;
	Gtk::EntryBuffer *entrybuffer_ = nullptr;
	Gtk::TextView *textview_ = nullptr;
	Gtk::ScrolledWindow *scrolledwindow_ = nullptr;
	Glib::RefPtr<Gtk::TextBuffer> textviewbuffer_;
	Glib::RefPtr<Gtk::Adjustment> adj_;
	std::string username = "";
private:
	void autoScroll();
	void printToBuffer();
};

#endif /* GUI_HPP_ */
