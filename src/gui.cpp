/*
 * gui.cpp
 *
 *  Created on: Mar 3, 2018
 *      Author: walaryne
 */

#include <iostream>
#include <mutex>
#include <thread>
#include "util.hpp"
#include "gui.hpp"
Client c;
std::mutex print_mu;

Gui::Gui(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& builder) : Gtk::Window(cobject) {
	c.dispatcher.connect(sigc::mem_fun(*this, &Gui::printToBuffer));
}

Gui::~Gui() {

}

void Gui::printToBuffer() {
	std::lock_guard<std::mutex> guard(print_mu);
	cnprint("Signaller called.");
	while(!c.IsPrintQueueEmpty()) {
		cnprint("Looped print queue.");
		std::string msg = c.SharedQueueGet();
		textviewbuffer_->insert_at_cursor(msg);
		autoScroll();
		c.SharedQueuePop();
	}
}

void Gui::autoScroll() {
	adj_->set_value(adj_->get_upper() - adj_->get_page_size());
}

void Gui::parseInput() {
	std::string text, host, port, textnl, finaltext;
    text = entry_->get_text();
    textnl = text + "\n";
    finaltext = username + ": " + textnl;
    entry_->set_text("");
    //textviewbuffer->insert_at_cursor(finaltext);
    if(text.find("/", 0) != std::string::npos) {
    	std::string textcpy = text;
    	std::string parsedtext = textcpy.erase(0, 1);
    	std::string command = textcpy.substr(0, parsedtext.find_first_of(" "));
    	std::cout << "Command is: " << command << "\n" << std::endl;
    	if(command == "connect") {
    		std::cout << "Connect command functioning" << "\n" << std::endl;
    		int firstlinebreak = parsedtext.find_first_of(" ");
    		int secondlinebreak = parsedtext.find(" ", firstlinebreak + 1);
    		int thirdlinebreak = parsedtext.find(" ", secondlinebreak + 1);
    		std::cout << firstlinebreak << "\n" << std::endl;
    		std::cout << secondlinebreak << "\n" << std::endl;
    		if(firstlinebreak == std::string::npos) {
    			std::cout << "Error at finding linebreak 1" << "\n" << std::endl;
    		}
    		if(secondlinebreak == std::string::npos) {
    			std::cout << "Error at finding linebreak 2" << "\n" << std::endl;
    		}
    		host = parsedtext.substr(firstlinebreak + 1, secondlinebreak - firstlinebreak -1);
    		if(firstlinebreak && (host != command)) {
    			std::cout << "Address is:" << host << "\n" << std::endl;
    		}
    		port = parsedtext.substr(secondlinebreak + 1);
    		if(firstlinebreak && (port != parsedtext)) {
    			std::cout << "Port is:" << port << "\n" << std::endl;
    		}
    		c.Initialize(host, port);
    		if(c.ConnectSocket(c.GetServerStruct())) {
    			//HAH! I implemented basic error checking, I'll give myself a cookie.
    			c.SetIsThreadRunning(true);
        		std::thread t([this](){ c.Receive(); });
        		t.detach();
        		c.SharedQueuePush("Successfully connected to " + host + ":" + port + "\n");
        		c.dispatcher();
    		}
    	}
    	if(command == "disconnect") {
    		c.DisconnectSocket();
    		c.SetIsThreadRunning(false);
    		c.SharedQueuePush("Successfully disconnected.\n");
    		c.dispatcher();
    	}
    	if(command == "username") {
    		int firstlinebreak = parsedtext.find_first_of(" ");
    		username = parsedtext.substr(firstlinebreak + 1);
    		c.SharedQueuePush("Username set to " + username + "\n");
    		c.dispatcher();
    	}
    	if(command == "register") {
    		c.SharedQueuePush(parsedtext);
    		c.dispatcher();
    		int firstlinebreak = parsedtext.find_first_of(" ");
    		std::string reformat = parsedtext.erase(0, firstlinebreak + 1);
    		c.Send(parsedtext.c_str(), 0x02);
    	}
    	if(command == "clear") {
    		textviewbuffer_->set_text("");
    	}
    	if(command == "help") {
    		c.SharedQueuePush("\n"
    				"Current commands: \n"
    				"/help\n"
    				"/connect <host> <port>\n"
    				"/username <username>\n"
    				"/disconnect\n"
    				"/clear\n"
    				);
    		c.dispatcher();
    	}
    	if(command == "exit") {
    		c.DisconnectSocket();
    		c.SetIsThreadRunning(false);
    		std::exit(0);
    	}
    } else {
        c.SharedQueuePush(finaltext);
        c.dispatcher();
    	c.Send(finaltext.c_str(), 0x01);
    }
}
