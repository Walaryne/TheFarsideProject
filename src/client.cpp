/*
 * client.cpp
 *
 *  Created on: Feb 26, 2018
 *      Author: walaryne
 */

#include <iostream>
#ifdef __WIN32__
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#endif
#include <string.h>
#include <stdio.h>
#include <mutex>
#include <unistd.h>
#include "client.hpp"
#include "util.hpp"
#include "encapsulator.hpp"

std::mutex queue_mu, queue_mu2;

int header_size = 6;

Encapsulator e(header_size);

Client::Client() {
	servinfo_ = nullptr;
	sockfd_ = 0;
	connected_ = false;
	running_ = false;
}

Client::~Client() {
	close(sockfd_);
#ifdef __WIN32__
	WSACleanup();
#endif
}

int Client::Initialize(std::string host, std::string port) {
	cnprint("Client::initialize called.");
	int status;
	addrinfo hints;
	addrinfo *servinfo;  // will point to the results

	memset(&hints, 0, sizeof hints); // make sure the struct is empty
	hints.ai_family = AF_UNSPEC;     // don't care IPv4 or IPv6
	hints.ai_socktype = SOCK_STREAM; // TCP stream sockets
	hints.ai_flags = AI_PASSIVE;     // fill in my IP for me

	if ((status = getaddrinfo(host.c_str(), port.c_str(), &hints, &servinfo)) != 0) {
	    fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
	    return 1;
	}
	servinfo_ = servinfo;
	return 0;
}

addrinfo* Client::GetServerStruct() {
	cnprint("Client::getserverstruct called.");
	return servinfo_;
}

bool Client::ConnectSocket(addrinfo *res) {
	sockfd_ = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if((connect(sockfd_, res->ai_addr, res->ai_addrlen)) == -1) {
		cnprint("Socket connection error.");
		return false;
	}
	return true;
}

int Client::Send(const char *msg, int keycode) {
	int len, bytes_sent;
	len = strlen(msg);
	try {
		std::string encapsulated = e.Encapsulate(msg, keycode);
		bytes_sent = send(sockfd_, encapsulated.c_str(), encapsulated.length(), 0);
	} catch(std::exception &e) {
		cnprinte(e.what());
	}
	std::cout << "Bytes sent: " << bytes_sent << "\n" << std::endl;
	return bytes_sent;
}

void Client::Receive() {
	char buf[MESSAGE_SIZE];
	while(running_) {
		if(int nbytes = recv(sockfd_, buf, header_size, 0) <= 0) {
			//socket closed from remote side
			if(nbytes == 0) {
				//socket closed gracefully
				this->SharedQueuePush("Connection closed from remote side\n");
				dispatcher();
			} else {
				std::cerr << "nbytes on error: " << nbytes << std::endl;
				perror("recv");
			}
			this->DisconnectSocket();
			running_ = false;
		} else {
			auto retrdata = e.Deencapsulate(buf);
			int remaining = retrdata.first;
			int keycode = retrdata.second;
			memset(&buf, 0, sizeof buf);
			std::cout << "Keycode: " << keycode << std::endl;
			recv(sockfd_, buf, remaining, 0);
			this->SharedQueuePush(std::string(buf));
			dispatcher();
			memset(&buf, 0, sizeof buf);
		}
	}
}

void Client::SharedQueuePush(std::string text) {
	std::lock_guard<std::mutex> guard(queue_mu);
	this->print_queue_.push_front(text);
}

void Client::SharedQueuePop() {
	std::lock_guard<std::mutex> guard(queue_mu2);
	this->print_queue_.pop_front();
}

bool Client::IsPrintQueueEmpty() {
	return this->print_queue_.empty();
}

std::string Client::SharedQueueGet() {
	return this->print_queue_.front();
}

bool Client::IsThreadRunning() {
	return running_;
}

void Client::SetIsThreadRunning(bool b) {
	running_ = b;
}

void Client::DisconnectSocket() {
#ifdef __WIN32__
	closesocket(sockfd_);
#else
	shutdown(sockfd_, SHUT_RDWR);
	close(sockfd_);
#endif
	running_ = false;
}
